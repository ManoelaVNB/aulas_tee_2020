// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyDQRmHjkIofP7Hmg3f6QccdHUCsoZF5ziA',
    authDomain: 'controle-if-tee.firebaseapp.com',
    databaseURL: 'https://controle-if-tee.firebaseio.com',
    projectId: 'controle-if-tee',
    storageBucket: 'controle-if-tee.appspot.com',
    messagingSenderId: '1061722000935',
    appId: '1:1061722000935:web:4942411ca3d7e9a9944802',
    measurementId: 'G-6TTW01TGP9'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
